define sublime_text::package::install(
	$package_name,
	$source
) {

	$params = split($name, ',')
	$user = $params[0]

	Exec { path => [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/', '/usr/local/bin/' ] }	

	if ($user != 'root') and ($user != 'Shared') {
		$packages_dir = "/Users/${user}/Library/Application Support/Sublime Text 3/Packages"

		if $source =~ /\.sublime-package$/ {
			$package_file = "${packages_dir}/${package_name}.sublime-package"
			exec { "download Sublime Text 3 package '${package_name}' for user ${user}":
				command => "wget -O '${package_file}' ${source}",
				creates => $package_file,
				require => File[$packages_dir]
			}
		} else {
			repository { "${packages_dir}/${package_name}":
				source => $source,
			}
		}
	}
}

define sublime_text::package(
	$source,
	$branch = 'master'
) {

	$package_name = $name

	if $users != '' {
		$users_split = split($users, "\n")

		$users_split_with_package = regsubst($users_split, "\$", ",${name}")

		package::install { $users_split_with_package:
			package_name => $name,
			source       => $source,
		}

	}

}
