class sublime_text {

	Exec { path => [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/', '/usr/local/bin/' ] }

	package { 'Sublime Text 3':
		ensure   => installed,
		provider => appdmg,
		source   => hiera('sublime_text::dmg', 'http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%20Build%203083.dmg'),
	}

	define user_prefs {
		if ($name != 'root') and ($name != 'Shared') {

			$sublime_dir = "/Users/${name}/Library/Application Support/Sublime Text 3"
			$sublime_package_dir = "${sublime_dir}/Packages"
			$sublime_user_package_dir = "${sublime_package_dir}/User"
			$sublime_installed_packages_dir = "${sublime_dir}/Installed Packages"

			$paths = [
				$sublime_dir,
				$sublime_package_dir,
				$sublime_user_package_dir,
				$sublime_installed_packages_dir
			]

			exec { "Sublime Text packages dir for ${name}":
				command => "mkdir -p '${sublime_installed_packages_dir}'",
				unless  => "test -e '${sublime_installed_packages_dir}'",
			}


			exec { "Sublime Text user dir for ${name}":
				command => "mkdir -p '${sublime_user_package_dir}'",
				unless  => "test -e '${sublime_user_package_dir}'",
			} ->

			file { $paths:
				ensure  => directory,
				owner   => "${name}"
			} ->

			exec { "installing bz2 for ${name}":
				command => "git clone https://github.com/codexns/sublime-bz2 '${sublime_package_dir}/bz2'",
				creates => "${sublime_package_dir}/bz2",
			} ->

			file { "Sublime Text prefs for user ${name}":
				ensure => present,
				path   => "${sublime_user_package_dir}/Preferences.sublime-settings",
				source => "puppet:///modules/sublime_text/Preferences.sublime-settings",
				owner  => "${name}",
			} -> 

			file { "Sublime Text ruby prefs for user ${name}":
				ensure => present,
				path   => "${sublime_user_package_dir}/Ruby.sublime-settings",
				source => "puppet:///modules/sublime_text/Ruby.sublime-settings",
				owner  => "${name}",
			}

		}
	}

	if $users != '' {
		$users_split = split($users, "\n")
		user_prefs { $users_split: }
	}

	# Install required font
	exec { 'installing Inconsolata font':
		command => 'wget -O /Library/Fonts/Inconsolata.otf http://www.levien.com/type/myfonts/Inconsolata.otf',
		creates => '/Library/Fonts/Inconsolata.otf',
		require => Package['wget'],
	}

}
